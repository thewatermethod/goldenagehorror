const striptags = require("striptags");
const entities = require("entities");

const getText = html => {
  return entities.decodeHTML(striptags(html));
};

export { getText };
