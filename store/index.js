const posts = [];

export const state = () => ({
  menuToggled: false,
});

export const mutations = {
  toggleMenu(state) {
    if (state.menuToggled) {
      state.menuToggled = false;
    } else {
      state.menuToggled = true;
    }
  },
};
