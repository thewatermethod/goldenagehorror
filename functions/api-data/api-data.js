/* eslint-disable */
const axios = require('axios');

let urlBase = `https://api.twilitgrotto.com/goldenagehorror/`;

/**
 * 
 */
const getAllHorror = async () => {
  let perPage = 100;
  let posts = getHorror({ perPage: perPage }, `posts`);
  let pages = getHorror({ perPage: perPage }, `pages`);

  return Promise.all([posts, pages]).then(values => {
    let postsArr = values[0];
    let pagesArr = values[1];
    return postsArr.concat(pagesArr); //[...postsArr, ...pagesArr]
  });
};

/**
 * 
 * @param {object} queryStringParameters 
 * @param {string} type 
 */
const getHorror = async (queryStringParameters, type) => {
  let query = '?';

  if (queryStringParameters.slug) {
    query += `slug=${queryStringParameters.slug}`;
  }

  if (queryStringParameters.perPage) {
    query += `per_page=${queryStringParameters.perPage}`;
  }

  let url = `${urlBase}wp-json/wp/v2/${type}${query}`;

  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(response => {
        let horror = response.data;

        if (horror.length == 0) {
          horror = getHorror(queryStringParameters, 'pages');
        }
        resolve(horror);
      })
      .catch(error => {
        console.log(error);
        return reject(error);
      });
  });
};

/**
 * 
 *  // the lambda handler
 *  @param {event} event 
 * 
 */

exports.handler = async event => {
  try {
    let horror = [];

    // ?all=true
    if (event.queryStringParameters.all === 'true') {
      horror = await getAllHorror();
    } else {
      // {slug, perPage}
      horror = await getHorror(event.queryStringParameters, 'posts');
    }

    return {
      statusCode: 200,
      body: JSON.stringify(horror),
    };
  } catch {
    return {
      statusCode: 500,
      body: 'There was an error',
    };
  }
};
