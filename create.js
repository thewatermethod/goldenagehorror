const writeFile = require("fs").writeFile;
const readFile = require("fs").readFile;
const axios = require("axios");
const find = require("lodash").find;

// const cat_com;
const tax = t => {
  return `<template>
  <div>
    <Category :tax="tax" />
  </div>
</template>

<script>

import Category from "../../components/category.vue";

export default {
  components: {
    Category
  },

  data() {
    return {
      tax: ${JSON.stringify(t)}
    };
  },

};
</script>`;
};

const component = post => {
  return `<template>
            <div>
              <Single :post="post" />
            </div>
          </template>

          <script>

          import Single from "../components/single.vue";

          export default {
            components: {
              Single
            },

            data() {
              return {
                post: ${JSON.stringify(post)}
              };
            },

          };
          </script>`;
};

const postMap = new Promise((resolve, reject) => {
  console.log("building post map");
  return axios
    .get(`https://www.goldenagehorror.com/.netlify/functions/api-data?all=true`)
    .then(res => {
      const postMap = [];
      console.log("creating category and tag key");
      let cats = [];
      let tags = [];
      /** first we make a map of all the posts */
      res.data.map(post => {
        const slug = post.slug;

        if (post.categories) {
          post.categories.forEach(category => {
            if (!cats[category]) {
              cats[category] = [];
            }

            cats[category].push(slug);
          });
        }

        if (post.tags) {
          post.tags.forEach(tag => {
            if (!tags[tag]) {
              tags[tag] = [];
            }

            tags[tag].push(slug);
          });
        }

        if (post["categories"] && post["categories"].indexOf(7) != -1) {
          postMap.push(slug);
        }
      });
      console.log("building a array of posts");
      console.log("....");
      /** write the map to a file */
      writeFile(`./static/posts.json`, JSON.stringify(postMap), err => {
        if (err) return console.error(err);
      });

      writeFile(
        `./static/cats.js`,
        `const cats = ${JSON.stringify(cats)}; export { cats };`,
        err => {
          if (err) return console.error(err);
        }
      );

      writeFile(
        `./static/tags.js`,
        `const tags = ${JSON.stringify(tags)}; export { tags };`,
        err => {
          if (err) return console.error(err);
        }
      );

      axios
        .get(`https://api.twilitgrotto.com/goldenagehorror/feed/podcast`)
        .then(res => {
          let feed = res.data.replace(
            /https\:\/\/api\.twilitgrotto\.com\/goldenagehorror/g,
            "https://www.goldenagehorror.com"
          );
          feed = feed.replace(
            `https://goldenagehorror.com/wp-content/uploads/2014/10/ITUNES-COVER.png`,
            `https://gahuploads.blob.core.windows.net/wp-content/2019/10/ITUNES-COVER.png`
          );
          console.log("....");
          console.log("building an rss feed");
          console.log("....");
          /** write the map to a file */
          writeFile(`./static/podcast/feed.rss`, feed, err => {
            if (err) return console.error(err);
          });
        });
      /** and then we break out all the routes */
      return res.data.map(post => {
        console.log("Building a json object for " + post.title.rendered);
        console.log("...");
        writeFile(
          `./static/routes/${post.slug}.json`,
          JSON.stringify(post),
          err => {
            if (err) return console.error(err);
          }
        );
        resolve(postMap);
      });
    });
});

const getTax = async (tax, arr, perPage, offset) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `https://api.twilitgrotto.com/goldenagehorror/wp-json/wp/v2/${tax}?per_page=${perPage}&offset=${offset}`
      )
      .then(t => {
        // process each tag
        t.data.forEach(tag => {
          arr.push(tag);
        });

        // writeFile(`./static/${tax}.js`, JSON.stringify(arr), err => {
        //   if (err) return console.error(err);
        // });
        resolve(arr);
      });
  });
};

const generate = new Promise((resolve, reject) => {
  axios
    .get(`https://www.goldenagehorror.com/.netlify/functions/api-data?all=true`)
    .then(res => {
      res.data.map(post => {
        console.log("creating vue template file for " + post.slug);
        console.log("...");
        writeFile(`./pages/${post.slug}.vue`, component(post), err => {
          if (err) return console.error(err);
        });
      });
    });
});

const createRedirects = redirects => {
  console.log("\n\n\n\n");
  console.log("...");
  console.log("building redirects ");
  let t = "";

  readFile("./static/_redirects.base", (err, data) => {
    if (err) throw err;

    t += data.toString("utf8");

    redirects.forEach(redirect => {
      t += "\n";
      t += redirect;
    });

    writeFile(`./static/_redirects`, t, err => {
      if (err) return console.error(err);
    });
  });

  //write to file
};

let cs = []; // categories
let ts = []; // tags

Promise.all([
  postMap,
  getTax("tags", ts, 100, 0),
  getTax("tags", ts, 100, 100),
  getTax("categories", cs, 100, 100)
]).then(() => {
  let redirects = [];
  cs.forEach((t, k, a) => {
    if (t.parent > 0) {
      const parent = find(a, { id: t.parent });
      //console.log(`${t.slug}'s parent is ${parent.slug}`);
      redirects.push(
        `/category/${parent.slug}/${t.slug}             /category/${t.slug}`
      );
      //add to redirect
    }

    // create component
    writeFile(`./pages/category/${t.slug}.vue`, tax(t), err => {
      if (err) return console.error(err);
    });
  });

  createRedirects(redirects);

  ts.forEach((t, k, a) => {
    //create component
    writeFile(`./pages/tag/${t.slug}.vue`, tax(t), err => {
      if (err) return console.error(err);
    }); 
  });

  generate;
});
