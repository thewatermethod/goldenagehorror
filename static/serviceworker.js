const cacheVersion = '0.1';
const staticCacheName = 'staticCache' + cacheVersion;

addEventListener('install', installEvent => {
  // console.log("Installed");
});

addEventListener('activate', activateEvent => {
  //console.log("Activated");
});

addEventListener('fetch', fetchEvent => {
  //console.log(fetchEvent);
});
