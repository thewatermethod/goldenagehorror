export default {

  target: 'static',
  
  // contents of the html head
  head: {
    htmlAttrs: {
      lang: "en-us"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        name: "robots",
        content: "max-snippet:-1, max-image-preview:large, max-video-preview:-1"
      },
      { property: "og:locale", content: "en_US" },
      { property: "og:type", content: "website" },
      {
        property: "google-site-verification",
        content: "JMYqnYFaETLVt-x3CnENArjN8QNRUjHyns4IYtQi-vI"
      }
    ],

    link: [
      { rel: "canonical", href: "https://goldenagehorror.com/" },
      {
        rel: "alternate",
        type: "application/rss+xml",
        title: "Podcast RSS",
        href: "https://goldenagehorror.com/feed/podcast"
      },
      {
        rel: "alternate",
        type: "application/rss+xml",
        title: "Sitemap",
        href: "https://goldenagehorror.com/sitemap.xml"
      }
    ]
  },

  modules: ["@nuxtjs/sitemap"],
  sitemap: {
    hostname: "https://www.goldenagehorror.com"
  },

  //static css for the page
  css: ["@/assets/styles.css"],

 
};
